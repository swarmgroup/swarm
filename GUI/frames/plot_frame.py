
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation

import importlib.util
spec = importlib.util.spec_from_file_location("parse_data", "Python_Parser/parser.py")
parser = importlib.util.module_from_spec(spec)
spec.loader.exec_module(parser)

##### Matplot functions -> should get their own script ######

def generate_data(data):
    """
    Generates dummy data.
    The elements will be assigned random initial positions and speed.
    Args:
        nbr_iterations (int): Number of iterations data needs to be generated for.
        nbr_elements (int): Number of elements (or points) that will move.
    Returns:
        list: list of positions of elements. (Iterations x (# Elements x Dimensions))
    """

    dims = (3,1)

    # Random initial positions.
    gaussian_mean = np.zeros(dims)
    gaussian_std = np.ones(dims)

    # Get parsed data from parser
    parserData = data
    finalDict = parserData[0]
    coordDict = parserData[1]

    parsedData = []
    coordList = []

    for key in finalDict:

        testX = coordDict[finalDict[key][";AnimCurveNode::T"]["translation|X"]].split(',')
        testY = coordDict[finalDict[key][";AnimCurveNode::T"]["translation|Z"]].split(',') # Swap Y and Z
        testZ = coordDict[finalDict[key][";AnimCurveNode::T"]["translation|Y"]].split(',')

        coordList.append([testX,testY,testZ])


    for index in range(0,len(coordList[0][0])):

        if index == 0 or index % 30 == 0: 

            current3DCoordinate = []
            for coord in coordList:
                current3DCoordinate.append([float(coord[0][index]),float(coord[1][index]),float(coord[2][index])])
            
        
            parsedData.append(np.array(current3DCoordinate))

    print(parsedData)
    return parsedData

def animate_scatters(iteration, data, scatters):
    """
    Update the data held by the scatter plot and therefore animates it.
    Args:
        iteration (int): Current iteration of the animation
        data (list): List of the data positions at each iteration.
        scatters (list): List of all the scatters (One per element)
    Returns:
        list: List of scatters (One per element) with new coordinates
    """
    for i in range(data[0].shape[0]):
        scatters[i]._offsets3d = (data[iteration][i,0:1], data[iteration][i,1:2], data[iteration][i,2:])
    return scatters

def main(data, save=False):
    """
    Creates the 3D figure and animates it with the input data.
    Args:
        data (list): List of the data positions at each iteration.
        save (bool): Whether to save the recording of the animation. (Default to False).
    """

    # Attaching 3D axis to the figure
    fig = plt.figure()
    ax = p3.Axes3D(fig)

    # Initialize scatters
    scatters = [ ax.scatter(data[0][i,0:1], data[0][i,1:2], data[0][i,2:]) for i in range(data[0].shape[0]) ]

    # Number of iterations
    iterations = len(data)

    # Setting the axes properties
    ax.set_xlim3d([-50, 50])
    ax.set_xlabel('X')

    ax.set_ylim3d([-50, 50])
    ax.set_ylabel('Y')

    ax.set_zlim3d([-50, 50])
    ax.set_zlabel('Z')

    ax.set_title('3D Animated Scatter Example')

    # Provide starting angle for the view.
    ax.view_init(25, 10)

    ani = animation.FuncAnimation(fig, animate_scatters, iterations, fargs=(data, scatters),
                                       interval=50, blit=False, repeat=True)

    return(fig, iterations)


#### Here starts the Tkinter part #####

class PlotFrame(ttk.Frame):
    def __init__(self,  container, **kwargs):
        super().__init__(container, **kwargs)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        
        input_frame = ttk.Frame(self, padding=10)
        input_frame.grid(row=1, column=0, sticky="EW")

        trip_upload = ttk.Button(
            input_frame,
            text="Upload",
            command=self.upload_trip_data
        )
        trip_upload.pack()

    def upload_trip_data(self):
        #tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
        filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        data = generate_data(parser.parse_data(filename))


        # Attaching 3D axis to the figure
        fig = plt.figure()
        ax = p3.Axes3D(fig)

        # Initialize scatters
        scatters = [ ax.scatter(data[0][i,0:1], data[0][i,1:2], data[0][i,2:]) for i in range(data[0].shape[0]) ]

        # Number of iterations
        iterations = len(data)

        # Setting the axes properties
        ax.set_xlim3d([-50, 50])
        ax.set_xlabel('X')

        ax.set_ylim3d([-50, 50])
        ax.set_ylabel('Y')

        ax.set_zlim3d([-50, 50])
        ax.set_zlabel('Z')

        ax.set_title('3D Animated Scatter Example')

        # Provide starting angle for the view.
        ax.view_init(25, 10)

        canvas = FigureCanvasTkAgg(fig, self)
        canvas.get_tk_widget().grid(row=0, column=0, sticky="EW")

        self.ani = animation.FuncAnimation(fig, animate_scatters, iterations, fargs=(data, scatters), interval=50, blit=False, repeat=True)    
