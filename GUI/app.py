import tkinter as tk
from frames.plot_frame import PlotFrame

class ControlCenter(tk.Tk):
    def __init__(self,  *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.geometry("1200x500")
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.plot_frame = PlotFrame(self)

        self.plot_frame.grid(row=0, column=0, sticky="NSEW")


root = ControlCenter()
root.mainloop()