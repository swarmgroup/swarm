from flask import Flask, render_template, redirect, request, url_for, jsonify, json
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, URL
from flask_ckeditor import CKEditor, CKEditorField
from datetime import date
import time
import math

# crazyflie imports
import cflib.crtp
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.swarm import CachedCfFactory
from cflib.crazyflie.swarm import Swarm
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie

import cflib.drivers.crazyradio as crazyradio


from swarmSequence import *
from Parser import Parser

app = Flask(__name__)
app.config['SECRET_KEY'] = '8BYkEfBA6O6donzWlSihBXox7C0sKR6b'
ckeditor = CKEditor(app)
Bootstrap(app)

##CONNECT TO DB
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///flights.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


##CONFIGURE TABLE
class SwarmFlight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), unique=True, nullable=False)
    subtitle = db.Column(db.String(250), nullable=False)
    date = db.Column(db.String(250), nullable=False)
    body = db.Column(db.Text, nullable=False)
    author = db.Column(db.String(250), nullable=False)
    img_url = db.Column(db.String(250), nullable=False)
# db.create_all()


##FLIGHT VARIABLES AND FUNCTIONS
parsed_data = Parser.Parser.parse_data()[0] # TODO: Das wird abschmieren, sobald man nicht mehr die fixe fbx hat...
flight_is_on = False
flight_iteration = 0


drones = {}

def check_positions_for_collision(position_list, iteration):
    for actual_pos in range(len(position_list)):
        for checking_pos in range(actual_pos + 1, len(position_list)):
            x_diff = abs(position_list[actual_pos][0] - position_list[checking_pos][0])
            y_diff = abs(position_list[actual_pos][1] - position_list[checking_pos][1])
            z_diff = abs(position_list[actual_pos][2] - position_list[checking_pos][2])

            distance_between_drones = math.sqrt(x_diff**2 + y_diff**2 + z_diff**2)

            if distance_between_drones < 0.3:
                print(50*"*")
                print("Error at iteration:", iteration)
                print("With distance:",  distance_between_drones)
                print("Between drones:", actual_pos, checking_pos)

                return True


def build_sequences(parsed_data, URI_set):
    #    x   y   z  time
    seq_args = {}
    counter = 0
    no_crash = True

    for uri in URI_set:
        seq_args[uri] = [[]]

    try:
        for iteration in parsed_data:
            if counter % 5 == 0:
                positions_for_crash_check = []
                for index in range(0, len(URI_set)):
                    sequence_entry = (iteration[index + 1][0], iteration[index + 1][1], iteration[index + 1][2] + 0.2, 5/24) # index shift because [0] gives timeinformation
                    seq_args[list(URI_set)[index]][0].append(sequence_entry)
                    positions_for_crash_check.append(sequence_entry)

                    if counter == 0:
                        print(sequence_entry)
                
                if check_positions_for_collision(positions_for_crash_check, iteration):
                    no_crash = False
            counter += 1

        return seq_args
    except Exception as e:
        return e


##WTForm
class CreateFlightForm(FlaskForm):
    title = StringField("Swarm Flight Title", validators=[DataRequired()])
    subtitle = StringField("Subtitle", validators=[DataRequired()])
    author = StringField("Your Name", validators=[DataRequired()])
    img_url = StringField("Blog Image URL", validators=[DataRequired(), URL()])
    body = CKEditorField("Blog Content", validators=[DataRequired()])
    submit = SubmitField("Submit Post")


@app.route('/')
def get_all_flights():
    flights = SwarmFlight.query.all()
    return render_template("index.html", all_flights=flights)


@app.route("/flight/<int:flight_id>")
def show_flight(flight_id):
    requested_flight = SwarmFlight.query.get(flight_id)

    # Get the parser data
    flight_data = Parser.Parser.parse_data()
    parsed_data = flight_data[0]
    time_information = flight_data[1]

    # build data for frontend
    data = {
        "flight": requested_flight,
        "parsed_data": parsed_data,
        "quantity_of_drones": len(parsed_data[0]) - 1,
        "time_information": time_information  # dict contains 'fps': frames per second and 'duration': total in seconds
        # duration in seconds
    }

    return render_template("flight.html", data=data)


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/contact")
def contact():
    return render_template("contact.html")


@app.route("/initialize-drones", methods=["GET"])
def initialize_drones():
    cflib.crtp.init_drivers(enable_debug_driver=False)
    
    possible_uris = [
        'radio://0/80/2M/E7E7E7E7E0',
        'radio://0/80/2M/E7E7E7E7E1',
        'radio://0/80/2M/E7E7E7E7E2',
        'radio://0/80/2M/E7E7E7E7E3'
    ]

    uris = {}
    try:
        crazyflie = cflib.crazyflie.Crazyflie()
    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})


    for uri in possible_uris:
        crazyflie.open_link(uri)
        time.sleep(1)
        status = crazyflie.state
        
        if status == 2:
            crazyflie.close_link()
            uris[uri] = "ok"
        else:
            uris[uri] = "not found"
            #uris[uri] = "ok" # testing

        
    return jsonify(uris)


@app.route("/start-drones-flight", methods=["POST"])
def start_drones_flight():
    available_drones = request.form.to_dict()
    uris = [] #set()
    available_drones_list = sorted(available_drones.items())

    #print(available_drones_list)

    for drone in available_drones_list:
        print(drone)
        if drone[1] == "activated":
            uris.append(drone[0])

    print(uris)

    seq_args = build_sequences(parsed_data, uris)


    cflib.crtp.init_drivers(enable_debug_driver=False)

    factory = CachedCfFactory(rw_cache='./cache')
    try:
        with Swarm(uris, factory=factory) as swarm:
            # If the copters are started in their correct positions this is
            # probably not needed. The Kalman filter will have time to converge
            # any way since it takes a while to start them all up and connect. We
            # keep the code here to illustrate how to do it.
            # swarm.parallel(reset_estimator)

            # The current values of all parameters are downloaded as a part of the
            # connections sequence. Since we have 10 copters this is clogging up
            # communication and we have to wait for it to finish before we start
            # flying.
            print('Waiting for parameters to be downloaded...')
            swarm.parallel(wait_for_param_download)

            swarm.parallel(run_sequence, args_dict=seq_args)
    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})



@app.route('/change-led', methods = ['POST'])
def change_led():
    try:
        data = request.form.to_dict()
        URI1 = (data["drone"])
        r = str(data["r"])
        g = str(data["g"])
        b = str(data["b"])
        mode = 7
        if "mode" in data:
            mode = data["mode"]
        if URI1 not in drones:
            drones[URI1] = SyncCrazyflie(URI1)
        com = LedCommander(mode, r, g, b, drones[URI1])

    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})

@app.route('/connect', methods = ['POST'])
def connect():
    try:
        data = request.form.to_dict()
        URI1 = (data["drone"])
        if URI1 not in drones:
            drones[URI1] = SyncCrazyflie(URI1)
        c = ConnectionCommander(drones[URI1], None)

    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})

@app.route('/disconnect', methods = ['POST'])
def disconnect():
    try:
        data = request.form.to_dict()
        URI1 = (data["drone"])
        if URI1 not in drones:
            drones[URI1] = SyncCrazyflie(URI1)
        c = ConnectionCommander(drones[URI1], None, True)

    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})

@app.route('/takeoff', methods = ['POST'])
def takeoff():
    try:
        data = request.form.to_dict()
        URI1 = (data["drone"])
        if URI1 not in drones:
            drones[URI1] = SyncCrazyflie(URI1)
        c = ConnectionCommander(drones[URI1], takeoff_connected)

    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})

def takeoff_connected(cf):
    # cf.commander.
    z = 0.4
    position = (0,0,z,0)
    take_off_time = 1.0
    sleep_time = 0.1
    steps = int(take_off_time / sleep_time)
    vz = position[2] / take_off_time

    print(vz)

    for i in range(steps):
        cf.cf.commander.send_velocity_world_setpoint(position[0], position[1], vz, position[3])
        time.sleep(sleep_time)


    print("pos")
    end_time = time.time() + 5
    while time.time() < end_time:
        cf.cf.commander.send_position_setpoint(0, 0, z, 10)
        time.sleep(0.5)
    print("End")


@app.route('/land', methods = ['POST'])
def land():
    try:
        data = request.form.to_dict()
        URI1 = (data["drone"])
        if URI1 not in drones:
            drones[URI1] = SyncCrazyflie(URI1)
        ConnectionCommander(drones[URI1], land_connected)

    except Exception as e:
        return jsonify({'status': 'Error', 'msg': str(e)})
    return json.dumps({'status': 'OK'})

def land_connected(cf):
    z = 0.2
    position = (0,0,z,0)
    landing_time = 1.0
    sleep_time = 0.1
    steps = int(landing_time / sleep_time)
    vz = -position[2] / landing_time

    print(vz)

    for _ in range(steps):
        cf.cf.commander.send_velocity_world_setpoint(0, 0, vz, 0)
        time.sleep(sleep_time)

    # cf.cf.commander.send_position_setpoint(position[0],
    #                                     position[1],
    #                                     position[2], 10)
    cf.commander.send_stop_setpoint()
    # Make sure that the last packet leaves before the link is closed
    # since the message queue is not flushed before closing
    time.sleep(0.1)


@app.route("/get-next-position/<int:iteration>", methods=["GET"])
def get_next_position(iteration):
    if iteration >= len(parsed_data):
        iteration %= len(parsed_data)

    next_position = parsed_data[iteration]

    # TODO: Hier noch die Kommunikation mit den Drohnen?

    return json.dumps({'status': 'OK', 'next_position': next_position})


@app.route("/new-flight", methods=["GET", "POST"])
def add_new_flight():
    form = CreateFlightForm()
    if form.validate_on_submit():
        new_flight = SwarmFlight(
            title=form.title.data,
            subtitle=form.subtitle.data,
            body=form.body.data,
            img_url=form.img_url.data,
            author=form.author.data,
            date=date.today().strftime("%B %d, %Y")
        )
        db.session.add(new_flight)
        db.session.commit()
        return redirect(url_for("get_all_flights"))
    return render_template("make-flight.html", form=form)


@app.route("/edit-flight/<int:flight_id>", methods=["GET", "POST"])
def edit_flight(flight_id):
    flight = SwarmFlight.query.get(flight_id)
    edit_form = CreateFlightForm(
        title=flight.title,
        subtitle=flight.subtitle,
        img_url=flight.img_url,
        author=flight.author,
        body=flight.body
    )
    if edit_form.validate_on_submit():
        flight.title = edit_form.title.data
        flight.subtitle = edit_form.subtitle.data
        flight.img_url = edit_form.img_url.data
        flight.author = edit_form.author.data
        flight.body = edit_form.body.data
        db.session.commit()
        return redirect(url_for("show_flight", flight_id=flight.id))
    return render_template("make-flight.html", form=edit_form, is_edit=True)


@app.route("/delete/<int:flight_id>")
def delete_flight(flight_id):
    flight_to_delete = SwarmFlight.query.get(flight_id)
    db.session.delete(flight_to_delete)
    db.session.commit()
    return redirect(url_for('get_all_flights'))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
