class Parser:
    def __init__(self):
        pass

    @staticmethod
    def parse_data():
        # maybe get the format by user
        final_dict = {}
        coord_dict = {}
        fps = 0

        # get keys by user -> GUI
        important_keys = [";AnimCurveNode::T", ";AnimCurveNode::R", ";AnimCurveNode::S"]

        # loop where the informations are taken
        with open('Parser/Swarm_Animation_V02_Export_ascii_.fbx') as file:
            nextline = False
            filtered_attribute = ""

            line = file.readline()
            while line:
                if 'CustomFrameRate' in line:
                    # Get the frame rate of the animation if -1 then the standard is 24 fps
                    fps = int(line.split(",")[-1])
                    if fps == -1:
                        fps = 24


                if 'AnimationCurve: ' in line:
                    # Extracts id of the animation curve
                    # current_animation_node_id = extractIdWithKeyFromLine('AnimationCurve: ', line)
                    start = line.find('AnimationCurve: ')
                    start = start + len('AnimationCurve: ')
                    end = line.find(',', start)
                    current_animation_node_id = line[start:end]
                    coord_dict[current_animation_node_id] = ""
                if "KeyValueFloat" in line:
                    do_read = True
                    substr = file.readline()
                    substr = substr.replace('\t', '')
                    substr = substr.replace("a: ", "")

                    current_line = substr
                    while do_read:
                        line = file.readline()
                        # leave, when } occurs
                        if "}" in line:
                            do_read = False
                            coord_dict[current_animation_node_id] = current_line.replace("\n", "")
                            break
                        else:
                            # We have the line of interest.
                            current_line = current_line + line

                # get dict with id, name, translation id, rotation id, scaling id
                if "drone_" in line or nextline:
                    if "drone_" in line:
                        # important information is in next line so flag is set to true
                        nextline = True

                        # check if id and name are in current line
                        drone_name = line.split("::")[1]
                        if "Mesh" in drone_name:
                            drone_id = line.split("Model: ")[1].split(",")[0]
                            drone_name = drone_name.split('",')[0]

                            final_dict[drone_id] = {"name": drone_name}

                        # check if important key is in current line, if yes -> set as filtered attribute and go to next line
                        else:
                            for important_key in important_keys:
                                if important_key in line:
                                    filtered_attribute = important_key

                    else:
                        # check if filtered attribute is set and if yes write it in dict
                        if filtered_attribute in important_keys:
                            line_list = line.split(",")
                            current_id = line_list[2]
                            filtered_value = line_list[1]

                            final_dict[current_id][filtered_attribute] = filtered_value

                            filtered_attribute = ""

                        # stop looking at the next line
                        nextline = False

                # get translation ids for x,y,z
                if ";AnimCurve::, AnimCurveNode::T" in line:

                    translation_dict = {}
                    line = file.readline()

                    line_list = line.split(",")
                    current_translation_id = line_list[2]
                    translation_dict["translationId"] = current_translation_id

                    translation_dict["translation|X"] = line_list[1]

                    line = file.readline()
                    line = file.readline()
                    line = file.readline()

                    translation_dict["translation|Y"] = line.split(",")[1]
                    line = file.readline()
                    line = file.readline()
                    line = file.readline()
                    translation_dict["translation|Z"] = line.split(",")[1]

                    for key, value in final_dict.items():
                        if value[";AnimCurveNode::T"] == current_translation_id:
                            final_dict[key][";AnimCurveNode::T"] = translation_dict

                line = file.readline()

        ### Reformat to [ [time, [x,y,z,light], [x,y,z,light], ... ], [...], ...  ]
        ###               |      |           |  |           |      |
        ###               |      \First drone/  \Second one /      |
        ###               |                                        |
        ###               \ First iteration with drone coord list  /

        parsed_data = []
        coord_list = []

        for key in final_dict:
            test_x = coord_dict[final_dict[key][";AnimCurveNode::T"]["translation|X"]].split(',')
            test_y = coord_dict[final_dict[key][";AnimCurveNode::T"]["translation|Z"]].split(',')  # Swap Y and Z
            test_z = coord_dict[final_dict[key][";AnimCurveNode::T"]["translation|Y"]].split(',')

            coord_list.append([test_x, test_y, test_z])

        # Duration for time values
        iterations = len(coord_list[0][0])
        duration = iterations / fps;

        for index in range(0, len(coord_list[0][0])):

            current_3d_coordinate = [(1 / fps)*(index+1)]
            for coord in coord_list:
                current_3d_coordinate.append([float(coord[0][index]), float(coord[1][index]), float(coord[2][index])])

            parsed_data.append(current_3d_coordinate)

        return [parsed_data, {'fps': fps, 'duration': duration}]
