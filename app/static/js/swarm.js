import * as THREE from './build/three.module.js';

import Stats from './stats.module.js';
import { GUI } from './dat.gui.module.js';

import { OrbitControls } from './OrbitControls.js' //'https://github.com/mrdoob/three.js/tree/master/examples/jsm/controls/OrbitControls.js';

let camera, scene, renderer, bulbLight, bulbMat, hemiLight, stats;
let ballMat, cubeMat, floorMat;

let bulbMat2;

let previousShadowMap = false;

let pub = {};
let container;

let lights = [];


let flightData;
let askedPosIndex = 0;
let lastAskedPosIndex = -1;
let lastTime = Date.now();
let canRequest = true;

var timer;

let dataFPS = 30;
let passedTime;
let currentTime = Date.now();
let startTime = Date.now(); // Time when play was pressed
var t = setInterval(updateLED, 2000);

let drones = new Array();





// ref for lumens: http://www.power-sure.com/lumens.htm
const bulbLuminousPowers = {
	"110000 lm (1000W)": 110000,
	"3500 lm (300W)": 3500,
	"1700 lm (100W)": 1700,
	"800 lm (60W)": 800,
	"400 lm (40W)": 400,
	"180 lm (25W)": 180,
	"20 lm (4W)": 20,
	"Off": 0
};

// ref for solar irradiances: https://en.wikipedia.org/wiki/Lux
const hemiLuminousIrradiances = {
	"0.0001 lx (Moonless Night)": 0.0001,
	"0.002 lx (Night Airglow)": 0.002,
	"0.5 lx (Full Moon)": 0.5,
	"3.4 lx (City Twilight)": 3.4,
	"50 lx (Living Room)": 50,
	"100 lx (Very Overcast)": 100,
	"350 lx (Office Room)": 350,
	"400 lx (Sunrise/Sunset)": 400,
	"1000 lx (Overcast)": 1000,
	"18000 lx (Daylight)": 18000,
	"50000 lx (Direct Sun)": 50000
};

const params = {
	shadows: true,
	exposure: 0.68,
	bulbPower: Object.keys(bulbLuminousPowers)[4],
	hemiIrradiance: Object.keys(hemiLuminousIrradiances)[0]
};









prepare();
init();
animate();






function prepareGui() {

	const gui = new GUI();

	gui.add(params, 'hemiIrradiance', Object.keys(hemiLuminousIrradiances));
	gui.add(params, 'bulbPower', Object.keys(bulbLuminousPowers));
	gui.add(params, 'exposure', 0, 1);
	gui.add(params, 'shadows');
	var conf = { color : '#ffae23' };
    gui.addColor(conf, 'color').onChange( function(colorValue) {
    console.log(colorValue)
    let rgb = hexToRgb(colorValue)
    let input = 'radio://0/80/2M/E7E7E7E7E1'
    changeLeds(input, Math.floor(rgb.r/255 * 100),Math.floor(rgb.g/255 * 100),Math.floor(rgb.b/255 * 100), 7)
});
	gui.open();

}
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}


function prepareGround() {
	floorMat = new THREE.MeshStandardMaterial({
		roughness: 0.8,
		color: 0xffffff,
		metalness: 0.2,
		bumpScale: 0.0005
	});
	const textureLoader = new THREE.TextureLoader();

	textureLoader.load("../static/img/white.jpg", function (map) {

		map.wrapS = THREE.RepeatWrapping;
		map.wrapT = THREE.RepeatWrapping;
		map.anisotropy = 4;
		map.repeat.set(10, 24);
		map.encoding = THREE.sRGBEncoding;
		floorMat.map = map;
		floorMat.needsUpdate = true;

	});

	const floorGeometry = new THREE.PlaneBufferGeometry(20, 20);
	const floorMesh = new THREE.Mesh(floorGeometry, floorMat);
	floorMesh.position.y = -0.1;
	floorMesh.receiveShadow = true;
	floorMesh.rotation.x = - Math.PI / 2.0;
	scene.add(floorMesh);
}




function prepare() {
	scene = new THREE.Scene();
	prepareGui();
	prepareGround();


	container = document.getElementById('threejs');

	stats = new Stats();
	container.appendChild(stats.dom);


	camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
	camera.position.x = - 4;
	camera.position.z = -40;
	camera.position.y = 60;
}



function init() {



	renderer = new THREE.WebGLRenderer();
	renderer.physicallyCorrectLights = true;
	renderer.outputEncoding = THREE.sRGBEncoding;
	renderer.shadowMap.enabled = true;
	renderer.toneMapping = THREE.ReinhardToneMapping;
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(0.8*window.innerWidth, 0.8*window.innerHeight);
	container.appendChild(renderer.domElement);


	const controls = new OrbitControls(camera, renderer.domElement);
	controls.minDistance = 1;
	controls.maxDistance = 200;

	window.addEventListener('resize', onWindowResize, false);




}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize(0.8*window.innerWidth, 0.8*window.innerHeight);

}

function animate() {

	requestAnimationFrame(animate);

	render();

}

function render() {

	renderer.toneMappingExposure = Math.pow(params.exposure, 5.0); // to allow for very bright scenes.
	renderer.shadowMap.enabled = params.shadows;

	if (params.shadows !== previousShadowMap) {

		//					ballMat.needsUpdate = true;
		//					cubeMat.needsUpdate = true;
		floorMat.needsUpdate = true;
		previousShadowMap = params.shadows;

	}

	renderer.render(scene, camera);

	stats.update();

}



/**
 * Function gets called, when on_start button is pressed.
 */
listener.on_start = function (time_information) {
    dataFPS = time_information.fps;
	canRequest = true;
	if (timer == undefined) {
		timer = setInterval(updateFunc, 100);
		lastTime = Date.now();
	}
	else {
		clearInterval(timer);
		timer = undefined;
	}
};


function get_next_position() {
	if (canRequest) {
		canRequest = false;
		get_flight_data(askedPosIndex, function (response) {
			lastAskedPosIndex = askedPosIndex;
			// calc next askedPosIndex
			let time = Date.now() - lastTime;
			time = Math.floor((time * dataFPS) / 1000);
			askedPosIndex = time;
			flightData = JSON.parse(response).next_position;
			canRequest = true;
		});
	}
}



function get_current_positions() {
	get_next_position();
	if (flightData != undefined) {
		return flightData;
	}
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function updateFunc() {
	let data = get_current_positions();
	if (data == undefined) {
	} else {
		let length = data.length - 1;
		if (lights.length < length) {
			for (let i = 0; i < length; i++) {
				createDrone(0, 0, 0);
			}
		}
		for (let i = 1; i < data.length; i++) {
			lights[i - 1].position.x = data[i][0];
			lights[i - 1].position.z = data[i][1];
			lights[i - 1].position.y = data[i][2];
		}
	}

}



function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}

function updateLED() {
	var ledIndex = getRandomInt(lights.length);
	if (ledIndex < lights.length) {
		var light = lights[ledIndex];
		light.changeColor(Math.random(), Math.random(), Math.random());
	}
}


function initDrone(x, y, z) {
	let drone = {};
	let emissive = 0xAA0000;
	drone.geometry = new THREE.SphereBufferGeometry(0.100, 10, 10);
	drone.material = new THREE.MeshStandardMaterial({
		emissive: emissive,
		emissiveIntensity: 10,
		color: emissive
	}, function getCol() {
		return emissive;
	});
	drone.light = new THREE.PointLight(emissive, 1, 100, 2);
	drone.light.add(new THREE.Mesh(drone.geometry, drone.material));
	drone.light.position.set(x, y, z);
	drone.light.castShadow = false;
	drone.light.emissive = drone.material.emissive;
	drone.light.direction = 1;
	drone.light.changeColor = function (r, g, b) {
		this.emissive.r = r;
		this.emissive.g = g;
		this.emissive.b = b;
		this.color.r = r;
		this.color.g = g;
		this.color.b = b;
	}

	return drone;
}


function createDrone(x, y, z) {
	let drone = initDrone(x, y, z);
	scene.add(drone.light);
	lights.push(drone.light);
}






window.pub = pub;
pub.drones = lights;
pub.createDrone = createDrone;




function changeLeds(input, r,g,b, mode){
    $.post( "/change-led", {
//          data: { "drone" : JSON.stringify(input) }//,r,g,b)
            drone : input,
            r : JSON.stringify(r),
            g : JSON.stringify(g),
            b : JSON.stringify(b),
            mode : JSON.stringify(mode)
        }, function(err, req, resp){
          console.log(resp["responseJSON"]);
        });
}
function takeoff(droneId){
    $.post( "/takeoff", {
            drone : droneId
        }, function(err, req, resp){
          console.log(resp["responseJSON"]);
        });
}
function land(droneId){
    $.post( "/land", {
            drone : droneId
        }, function(err, req, resp){
          console.log(resp["responseJSON"]);
        });
}
function connect(droneId){
    $.post( "/connect", {
            drone : droneId
        }, function(err, req, resp){
          console.log(resp["responseJSON"]);
        });
}
function disconnect(droneId){
    $.post( "/disconnect", {
            drone : droneId
        }, function(err, req, resp){
          console.log(resp["responseJSON"]);
        });
}
//pub.test = testMethod();


/* test-code */
if(window.api === undefined)
{
window.api = {"__testonly__" : {}};
}
window.api.__testonly__.changeLeds = changeLeds;
window.api.__testonly__.takeoff = takeoff;
window.api.__testonly__.land = land;
window.api.__testonly__.connect = connect;
window.api.__testonly__.disconnect = disconnect;
window.a = window.api.__testonly__;
/* end-test-code */