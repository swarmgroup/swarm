

def parse_data(path):

    #maybe get the format by user
    finalDict = {}
    coordDict = {}

    # get keys by user -> GUI
    importantKeys = [";AnimCurveNode::T",";AnimCurveNode::R",";AnimCurveNode::S"]

    # loop where the informations are taken
    with open(path) as file:
        nextline = False
        filteredAttribute = ""

        line = file.readline()
        while line:

            if 'AnimationCurve: ' in line:
                # Extrahiert die ID der AnimationCurve
                # currentAnimationNodeID = extractIdWithKeyFromLine('AnimationCurve: ', line)
                start = line.find('AnimationCurve: ')
                start = start + len('AnimationCurve: ')
                end = line.find(',', start)
                currentAnimationNodeID = line[start:end]
                coordDict[currentAnimationNodeID] = ""
            if "KeyValueFloat" in line:
                doRead = True
                #print(currentAnimationNodeID + "\n")
                substr = file.readline()
                substr = substr.replace('\t', '')
                substr = substr.replace("a: ", "")
                #print(substr)
                currentLine = substr
                while doRead:
                    line = file.readline()
                    # leave, when } occurs
                    if "}" in line:
                        doRead = False
                        coordDict[currentAnimationNodeID] = currentLine.replace("\n","")
                        break
                    else:
                        # We have the line of interest.
                        #print(line)
                        currentLine = currentLine + line




            # get dict with id, name, translation id, rotation id, scaling id
            if "drone_" in line or nextline:
                if "drone_" in line:
                    # important information is in next line so flag is set to true
                    nextline = True

                    # check if id and name are in current line
                    droneName = line.split("::")[1]
                    if "Mesh" in droneName:
                        droneId = line.split("Model: ")[1].split(",")[0]
                        droneName = droneName.split('",')[0]
                        
                        finalDict[droneId] = {"name": droneName}

                    # check if important key is in current line, if yes -> set as filtered attribute and go to next line
                    else:
                        for importantKey in importantKeys:
                            if importantKey in line:
                                filteredAttribute = importantKey
                        
                
                else:
                    # check if filtered attribute is set and if yes write it in dict
                    if filteredAttribute in importantKeys:
                        lineList = line.split(",")
                        currentId = lineList[2]
                        filteredValue = lineList[1]

                        finalDict[currentId][filteredAttribute] = filteredValue

                        filteredAttribute = ""

                    # stop looking at the next line
                    nextline = False

            # get translation ids for x,y,z
            if ";AnimCurve::, AnimCurveNode::T" in line:
                
                translationDict = {}
                line = file.readline()

                lineList = line.split(",")
                currentTranslationId = lineList[2]
                translationDict["translationId"] = currentTranslationId

                translationDict["translation|X"] = lineList[1]
                
                line = file.readline()
                line = file.readline()
                line = file.readline()

                translationDict["translation|Y"] = line.split(",")[1]
                line = file.readline()
                line = file.readline()
                line = file.readline()
                translationDict["translation|Z"] = line.split(",")[1]

                for key, value in finalDict.items():
                    if value[";AnimCurveNode::T"] == currentTranslationId:
                        finalDict[key][";AnimCurveNode::T"] = translationDict



            line = file.readline()


    return [finalDict,coordDict]

